[![Build Status](https://gitlab.com/chrysn/coap-message-utils/badges/master/pipeline.svg)](https://gitlab.com/chrysn/coap-message-utils/commits/master)
![Maintenance](https://img.shields.io/badge/maintenance-experimental-blue.svg)

# coap-message-utils

CoAP message implementation tools

This crate contains utilities for creating own implementations of the [coap_message] traits, as
well as some basic implementations that may be useful for CoAP libraries.

The implementations aim for being easy and straightforward. While they are largely usable on
embedded systems, it is expected that more optimized versions are used in that area when byte
comes to shove.

License: MIT OR Apache-2.0
