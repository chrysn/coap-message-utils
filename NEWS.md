# Changes in 0.3.0-alpha.1

* Switched to coap-message 0.3.0-alpha.1 and its fallible writing;
  this adds the WriteError type that expresses the cause.

* Introduced InternalServerError 

* Intorduced a `reset()` method on the writable message as a stopgap measure
  until rewindable message traits are available.
