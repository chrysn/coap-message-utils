#![no_std]
//! CoAP message implementation tools
//!
//! This crate contains utilities for creating own implementations of the [coap_message] traits, as
//! well as some basic implementations that may be useful for CoAP libraries.
//!
//! The implementations aim for being easy and straightforward. While they are largely usable on
//! embedded systems, it is expected that more optimized versions are used in that area when byte
//! comes to shove.

pub mod option_extension;
pub mod option_iteration;

pub mod inmemory;
pub mod inmemory_write;

pub mod error;

#[cfg(test)]
mod test;
