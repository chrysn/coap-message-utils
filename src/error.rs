//! Generally helpful error types for use with [coap_message]

use coap_message::error::RenderableOnMinimal;
use coap_message::MinimalWritableMessage;

/// A fall-back error that indicates that nothing more could go wrong.
///
/// This reports as 5.00, and is useful as an eventual error type when not even creating a good
/// error is possible or practical any more.
///
/// TBD: It'd be convenient if any error could be made to convert `Into<InternalServerError>`, but
/// the From/Into duality and orphan rules forbid that.
#[derive(Debug)]
pub struct InternalServerError;

impl RenderableOnMinimal for InternalServerError {
    type Error<IE: RenderableOnMinimal + core::fmt::Debug> = Self;

    fn render<M: MinimalWritableMessage>(self, msg: &mut M) -> Result<(), Self> {
        let code = coap_numbers::code::INTERNAL_SERVER_ERROR
            .try_into()
            .map_err(|_| InternalServerError)?;
        msg.set_code(code);
        Ok(())
    }
}

/// Error type for manipulation of [crate::inmemory_write] messages.
///
/// This is presented externally as an InternalServerError, but has enough structure to be useful
/// for debugging through `dbg!()`.
#[derive(Debug)]
#[non_exhaustive]
pub enum WriteError {
    OutOfSequence,
    OutOfSpace,
}

impl RenderableOnMinimal for WriteError {
    type Error<IE: RenderableOnMinimal + core::fmt::Debug> = crate::error::InternalServerError;
    fn render<M: coap_message::MinimalWritableMessage>(
        self,
        msg: &mut M,
    ) -> Result<(), Self::Error<M::UnionError>> {
        crate::error::InternalServerError.render::<M>(msg)
    }
}

impl From<core::convert::Infallible> for WriteError {
    fn from(never: core::convert::Infallible) -> Self {
        match never {}
    }
}
